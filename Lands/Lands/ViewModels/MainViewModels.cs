﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lands.ViewModels
{
    class MainViewModels
    {
        #region ViewsModels
        public LoginViewModel Login
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        public MainViewModels()
        {
            this.Login = new LoginViewModel();
        }

        #endregion
    }
}
